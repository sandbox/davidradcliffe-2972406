Copy to clipboard
=================

This module makes it easy to create a 'copy to clipboard' button for any input element.
Simply add a button with class "clipboard-copy-button" as a sibling of the input element,
and add the class name "clipboard-copy-target" to the input elements.
The class names can be changed in configuration at
/admin/config/user-interface/copy-to-clipboard.

The clipboardjs module (https://www.drupal.org/project/clipboardjs) offers similar
functionality, but it adds a copy to clipboard function for text fields.

## Usage example

This HTML snippet displays an invite link for the
invite module (https://www.drupal.org/project/invite_link)
along with a button to copy the link to the clipboard.

     <p>
       Invite link:
       <input type="text" size=40 readonly="readonly" class="clipboard-copy-target"
              value="[site:url]/invite/code/[current-user:uid]" />
       <button class="clipboard-copy-button">Copy</button>
     </p>
