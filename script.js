jQuery(document).ready(function () {
  var buttonClass = Drupal.settings.copyButtonClass.toString(),
      targetClass = Drupal.settings.copyTargetClass.toString();
  if (buttonClass && targetClass) {
    jQuery('.' + buttonClass).click(function() {
      var input = this.parentElement.querySelector('.' + targetClass);
      if (input !== null) {
        input.select();
        document.execCommand('Copy');
      }
    });
  }
});
